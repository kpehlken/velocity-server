const express = require('express');
const productRoutes = require('./routes/product');
const authRoutes = require('./routes/userAuth');
const deliveryRoutes = require('./routes/delivery');
const driverRoutes = require('./routes/driver');
const userRoutes = require('./routes/user');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const Driver = require('./models/Driver');
const cors = require('cors');
const morgan = require('morgan');
const fs = require('fs');

 // Handle CORS


/* Config Area */

const PORT = process.env.PORT || 5000;

// Config .env
require('dotenv').config();

// Connect to MongoDB Atlas Test Cluster
mongoose.connect(process.env.DB_URI, { useNewUrlParser: true, useFindAndModify: false }, () => {
    console.log("[+] Connect to MongoDB Test Cluster (DB: Velocity)");
});

// Create app instance
const app = express();

// Create HTTP Server due to socket.io
const httpServer = require('http').createServer(app);

// Tell express to use JSON
app.use(express.json());

app.use(cors());
app.use(morgan('common', {
    stream: fs.createWriteStream('./access.log', {flags: 'a'})
}));

// Load routes from routes folder
app.use('/products', productRoutes);
app.use('/user', authRoutes);
app.use('/user/profile', userRoutes);
app.use('/delivery', deliveryRoutes);
// Protected with User Roles
app.use('/driver', driverRoutes);

httpServer.listen(PORT, () => {
    console.log(`[+] Server started on PORT ${PORT}`);
});


// Socket.io
let connections = {};

const io = require('socket.io')(httpServer, { cors: { origin: ['http://localhost:8080', 'http://localhost:5000'] } });

io.use((socket, next) => {
    // Get Token from socket 
    const jwtToken = socket.handshake.auth.token;
    
    // Verify Token
    jwt.verify(jwtToken, process.env.TOKEN_SECRET, (err) => {
        // If Invalid return Error
        if(err) return next(new Error("Invalid Token"));

        // Set token property of socket to valid JWT Token
        socket.token = jwtToken;

        // End Middleware
        next();
    });
});

io.on("connection", (socket) => {
    // DEV or change into Logging to File
    console.log(socket.id + " connected!")

    // DEV store socket in Redis Cache
    connections["John"] = socket;

    // 
    socket.on('subscribe', () => {
        // Get token payload
        const payload = jwt.decode(socket.token);

        // Check if already connected || Socket already in Redis
        if(Object.keys(connections).find(key => connections[key] === socket)) return new Error("Already connected");

        // Lookup Driver with this ID in DB
        Driver.find({ _id: payload._id }).then((driver, err) => {
            // Handle error if Driver was not found (So Token payload is invalid)
            if(err || driver.name === undefined) return new Error("Driver not found");
        
            // Save socket in Redis with key: driver.name
            connections[driver.name] = socket;

            //Dev
            console.log(`${driver.name} is ${socket.id}`);
        });
    });

    socket.on("disconnect", () => {
        console.log(Object.keys(connections).find(key => connections[key] === socket.id) + " disconnected!");

        let disconnectedUser = Object.keys(connections).find(key => connections[key] === socket.id);
        connections[disconnectedUser] = undefined;
        driverSocket = undefined;
    });
});
