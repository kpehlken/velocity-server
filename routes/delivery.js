const express = require('express');
const router = express.Router();
const authenticate = require('../middleware/auth');
const { validateCourierRequest, validateRetourRequest, validateForgottenRequest, validateReceiptRequest, validateCustomRequest } = require('../middleware/validation');
const Order = require('../models/Order');
const CompletedOrder = require('../models/CompletedOrder');
const jwt = require('jsonwebtoken');
const informDriver = require('../functions/driver');
const sendSms = require('../apis/twilio');

router.get('/standard', authenticate, (req, res) => {
    // Lookup all currently available services

    // res.json(services)

    // Handle errros
});

router.post('/custom/request', authenticate, (req, res) => {
    // Check Body for mailcious formats
    let isSuccessFul = false;
    const PAYLOAD = jwt.decode(req.headers.authorization.split(" ")[1]);
    const USERID = PAYLOAD._id || PAYLOAD.guestId;

    let isApproved = false;
    try {
        const { error } = validateCustomRequest(req.body);
        if(error) return res.status(400).json({ message: error.message });

        // Notify ADMIN
        sendSms(`[ACHTUNG!] [ADMIN] Es gibt eine benutzerdefinierte Anfrage! ${req.body.description} und Adresse: ${req.body.address}`, "+19419607429", "+4915751913926");

        // WAIT FOR APPROVAL
        // Check DB if some FLAG is set
        isApproved = true;

    } catch(err) {
        return res.status(500).json({ success: false });
    }
 
    // res.json(yes/no, ETA, ...)
    res.json({
        isSuccessFul,
        approved: isApproved,
        ETA: 30
    })
});

router.post('/receipt', authenticate, (req, res) => {
    let isSuccessFul = false;
    const PAYLOAD = jwt.decode(req.headers.authorization.split(" ")[1]);
    const USERID = PAYLOAD._id || PAYLOAD.guestId;
    console.log("INSERT ID: " + USERID)
    try {
        const { error } = validateReceiptRequest(req.body);
        if(error) return res.status(400).json({ message: error.message });

        informDriver(`Bitte fahren Sie nach: ${req.body.location}`);

        let completedOrder = new CompletedOrder({
            userId: USERID,
            details: req.body,
            completionDate: new Date(Date.now()),
            orderType: "Rezept vom Arzt"
        });
        completedOrder.save();

        isSuccessFul = true;
    } catch(err) {
        // Log error
        isSuccessFul = false;
    }

    res.json({
        success: isSuccessFul
    });

});

router.post('/forgotten', authenticate, (req, res) => {
    let isSuccessFul = false;
    const PAYLOAD = jwt.decode(req.headers.authorization.split(" ")[1]);
    const USERID = PAYLOAD._id || PAYLOAD.guestId;
    try {
        const { error } = validateForgottenRequest(req.body);
        if(error) return res.status(400).json({ message: error.message});

        informDriver(`Eine neue Bestellung! Bitte fahren sie zu dem Ort der dieser Beschreibung passt: ${req.body.location}. Und suchen Sie nach: "${req.body.item}". Wenn Sie es gefunden haben bringen Sie es nach: ${req.body.address}!`);

        let completedOrder = new CompletedOrder({
            userId: USERID,
            details: req.body,
            completionDate: new Date(Date.now()),
            orderType: "Vergessener Gegenstand"
        });
        completedOrder.save();

        isSuccessFul = true;

    } catch(err) {
        // Log error
        isSuccessFul = false;
    }

    res.json({
        success: isSuccessFul
    });

});

router.post('/retour', authenticate, (req, res) => {
    //Body: [ weight, address ]
    let isSuccessFul = false;
    const PAYLOAD = jwt.decode(req.headers.authorization.split(" ")[1]);
    const USERID = PAYLOAD._id || PAYLOAD.guestId;
    try {
        const { error } = validateRetourRequest(req.body);
        if(error) return res.status(400).json({ message: error.message });

        informDriver(`Fahren Sie nach ${req.body.address} und holen Sie das Paket ab (Gewicht: ${req.body.weight}kg) und bringen Sie es zur Post als Retour!`);

        let completedOrder = new CompletedOrder({
            userId: USERID,
            details: req.body,
            completionDate: new Date(Date.now()),
            orderType: "Retour zur Post"
        });
        completedOrder.save();

        isSuccessFul = true;
    } catch(err) {
        isSuccessFul = false;
    }

    res.json({
        success: isSuccessFul
    });

});

/* Sofort oder Standard Lieferung */
router.post('/courier', authenticate, (req, res) => {
    // Validate Data of request (Body: name, userAddress, destAddress, description, weight, addressinfo, ...?)
    const { error } = validateCourierRequest(req.body);
    if(error) return res.status(400).json({ message: error.message });

    const PAYLOAD = jwt.decode(req.headers.authorization.split(" ")[1]);
    const USERID = PAYLOAD._id || PAYLOAD.guestId;

    if(!req.body.instantDelivery) {
        delete req.body.instantDelivery;
        // Add to Queue/Array
        let order = new Order({
            type: "courier",
            orderAt: Date.now(),    // Unprecise?
            details: req.body
        });
        order.save();
        return res.status(200).json({ message: "Wir kommen um 13:00 oder 18:00 Uhr bei Ihnen vorbei!" });
    } 
    console.log(req.body);
    const DRIVER = informDriver(`Ein neuer Kurierauftrag! Fahren Sie zu ${req.body.userAddress} (Bei ${req.body.addressInfo.split(",")[0]} klingeln) und holen Sie den Gegenstand ab mit dieser Beschreibung: "${req.body.description}". Bringen Sie diesen Gegenstand zu: ${req.body.destAddress} und bringen Sie es zu ${req.body.addressInfo.split(",")[1]}!`);
   
    let completedOrder = new CompletedOrder({
        userId: USERID,
        details: req.body,
        completionDate: new Date(Date.now()),
        orderType: "Kurierauftrag"
    });
    completedOrder.save();

    // res.json({ success, message, ETA })
    res.json({
        success: true,
        message: "Fahrer ist unterwegs",
        userETA: 30,
        destETA: 30,
        driverName: DRIVER
    })
});

module.exports = router;