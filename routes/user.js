const express = require('express');
const router = express.Router();
const authenticate = require('../middleware/auth');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const CompletedOrder = require('../models/CompletedOrder');

/*
    Profile Route. Returns Data for identifying guest or user on account.vue
*/
router.get('/', authenticate, async (req, res) => {
    console.log("Profile Route accessed");
    const payload = jwt.decode(req.headers["authorization"].split(" ")[1]);
    console.log(payload);

    // If Guest is logged in
    if(payload.guest) {
        res.json({ guest: true });
    } else {
        // If User is registered
        let user = await User.findOne({ _id: payload._id });
        if(!user) return res.status(400).json({ message: "Es wurde kein Benutzer mit diesen Daten gefunden" });

        user.password = undefined;

        res.json({ user });
    }
});

router.get("/orders", authenticate, (req, res) => {
    console.log("/orders Route accessed")
    const PAYLOAD = jwt.decode(req.headers.authorization.split(" ")[1]);
    console.log(PAYLOAD)
    const ID = PAYLOAD._id || PAYLOAD.guestId;

    CompletedOrder.find({ userId: ID }).then(orders => {
        res.json({
            orders
        })
    });
});

module.exports = router;