const express = require('express');
const router = express.Router();

/* AUTH as Driver */

/*
    All active socktes are stored in Redis (Instead of connections object)
*/


router.get('/', (req, res) => {
    const connections = require('../index');    // Replace with Redis

    let socket = connections["John"];   // redis.get(driver.name)
    socket.emit("message", "Hiiii") // Emit data

    res.json({ message: "Driver Route" })
});

router.post('init', (req, res) => {
    
});

module.exports = router;