const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const authenticate = require('../middleware/auth');
const rateLimit = require('express-rate-limit');
const { validateRegistrationRequest, validateLoginRequest, validateNewTokenRequest, validateLogoutRequest } = require('../middleware/validation');
const User = require('../models/User');
const Token = require('../models/Token');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');

// Rate Limiter Config
const tokenLimiter = rateLimit({
    windowMs: 30 * 60 * 1000,
    max: 100
});

const loginLimiter = rateLimit({
    windowMs: 60 * 60 * 1000,
    max: 8,
    message: "Es wurden zu viele Loginversuche von diesem Gerät versucht!"
});

router.post('/register', async (req, res) => {
    // Validate Data of request
    const { error } = validateRegistrationRequest(req.body);
    if(error) return res.status(400).json({ message: error.message });

    // Check if User already exists in DB
    const isExisting = await User.findOne({ email: req.body.email });
    if(isExisting) return res.status(400).json({ message: "Es existiert bereits ein Konto mit dieser Email" });

    // Hash Password
    const salt = await bcrypt.genSalt(10);
    let hashedPassword = await bcrypt.hash(req.body.password, salt);

    // Create User Object
    let user = new User({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        password: hashedPassword,
        email: req.body.email,
        address: req.body.address
        // Card Info?
    });

    try {
        // Save to MongoDB
        const savedUser = await user.save();
    
        // Sign JWT Token (expires in 30min) with User id
        const token = jwt.sign({ _id: savedUser._id }, process.env.TOKEN_SECRET, { expiresIn: '30min' });

        // Sign JWT Refresh Token with User id
        const refreshToken = jwt.sign({ _id: savedUser._id }, process.env.REFRESH_TOKEN);

        // res.json(token, refreshToken)
        res.json({
            token,
            refreshToken
        });

    } catch(err) {
        console.log("[-] Error occured!", err);
        res.json({ message: err });
    }
});

router.post('/guest', async (req, res) => {
    let guestId = crypto.randomBytes(12).toString('hex');

    const jwtToken = jwt.sign({ guest: true, guestId }, process.env.TOKEN_SECRET, { expiresIn: '60min' });

    const refreshToken = jwt.sign({ guest: true, _id: guestId }, process.env.REFRESH_TOKEN);

    // Save refreshToken
    // [Store refreshToken in Array or Database (Redis Cache)]
    try {
        let storedToken = new Token({refreshToken});
        storedToken.save();
    } catch(err) {
        console.log("[-] Error occured! ", err);
        res.json({ message: err});
    }

    res.json({ token: jwtToken, id: guestId, refreshToken: refreshToken });
});

router.post('/login',  async (req, res) => {
    // Validate Data of request
    const { error } = validateLoginRequest(req.body);
    if(error) return res.status(400).json({ message: error.message });

    // Check if email exists
    const user = await User.findOne({ email: req.body.email });
    if(!user) return res.status(400).json({ message: "Es gibt keinen Benutzer mit dieser Email" });

    // Password Check
    const isValid = await bcrypt.compare(req.body.password, user.password);
    if(!isValid) return res.status(400).json({ message: "Ungültige Email oder ungültiges Passwort" });

    // Sign a JWT Token (expires in 30min) with User id
    const jwtToken = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET, { expiresIn: '30min' })

    // Sign a JWT Refresh Token with User id
    const refreshToken = jwt.sign({ _id: user._id }, process.env.REFRESH_TOKEN)

    // [Store refreshToken in Array or Database (Redis Cache)]
    try {
        let storedToken = new Token({refreshToken});
        storedToken.save();
    } catch(err) {
        console.log("[-] Error occured! ", err);
        res.json({ message: err});
    }

    // Send wanted response
    res.json({
        token: jwtToken,
        refreshToken: refreshToken
    });
});

router.post('/token', tokenLimiter, async (req, res) => {
    // Validate Request
    const { error } = validateNewTokenRequest(req.body);
    if(error) return res.status(400).json({ message: error.message });

    // get refreshToken from body
    const { refreshToken } = req.body;

    if(refreshToken == null) return res.sendStatus(401);

    let payload = jwt.decode(req.body.refreshToken);
    console.log(payload);

    // Check if refreshToken is valid by looking up in Database
    // Only return 403 if refreshToken is not it database   
    const isExisting = await Token.findOne({ refreshToken: req.body.refreshToken });
    if(!isExisting) return res.status(403).json({ message: "Bitte loggen Sie sich erneut an" });

    // Verify token
    jwt.verify(refreshToken, process.env.REFRESH_TOKEN, (err, data) => {
        console.log(data);
        // Handle errors
        if(err) return res.sendStatus(403);
        // Generate access Token 
        let jwtToken = ""
        if(data.guest) {
            console.log("[!] Is Guest")
            jwtToken = jwt.sign({ guest: true, guestId: data.guestId }, process.env.TOKEN_SECRET, { expiresIn: '30min' });
        }
        jwtToken = jwt.sign({ _id: data._id }, process.env.TOKEN_SECRET, { expiresIn: '30min' });
        console.log("New: " + jwtToken)
        res.json({ token: jwtToken });
    });

    // Handle errors (try catch)
});

/*
    Prefix: /user
*/
router.delete('/logout', authenticate, (req, res) => {
    // Validate Data of Request
    const { error } = validateLogoutRequest(req.body);
    if(error) return res.status(400).json({ message: error.message });

    // Delete refreshToken from Database
    Token.findOneAndRemove({ refreshToken: req.body.refreshToken }).then((data, err) => {
        if(err) return res.status(400).json({ message: err });
        res.json({ message: "Erfolgreich abgemeldet!" });
    })
});

module.exports = router;