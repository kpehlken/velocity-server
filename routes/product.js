const express = require('express');
const router = express.Router();
const authenticate = require('../middleware/auth');
const Product = require('../models/Product');
const { validateProductSearchRequest, validatePurchaseRequest } = require('../middleware/validation');
const Stripe = require('stripe');
const informDriver = require('../functions/driver');
const CompletedOrder = require('../models/CompletedOrder');
const stripe = Stripe('sk_test_51J3hkgHtpxMpJiE0WP1dcR2DTWaUQF6SnIvMA3nSY0rDDoMv2tvGfosYgKv4aPDaxMnTznuzXVdhOZnGnMtmoD8300KHnmwXeY');
const jwt = require("jsonwebtoken");

router.get('/:id', authenticate, (req, res) => {
    Product.find({ _id: req.params.id }).then(product => {
        res.json(product)
    });
});

router.get('/search/:term', async (req, res) => {
    // Check term for malicious format
    const { error } = await validateProductSearchRequest(req.params);   // Useless?
    if(error) return res.status(400).json({ message: error.message });

    res.json({
        message: "This Route is not working."
    })
});

router.get('/category/:category', async (req, res) => {
    // Check :category for malicious format
    // Check against hardcoded array of all categories

    
    // Send Response
    res.json({
        message: "This Route is not working."
    });
});

/*
    Called to create Payment Intent (Stripe), handle Driver, ...
*/
router.post('/buy', authenticate, async (req, res) => {

    // Body: [paymentMethod, articles ]

    // Validate somehow shopping cart (req.body) e.g. check if all included articles really exist in DB
    const { error } = await validatePurchaseRequest(req.body);   // Useless?
    if(error) return res.status(400).json({ message: error.message });
    
    const PAYLOAD = jwt.decode(req.headers.authorization.split(" ")[1]);
    const USERID = PAYLOAD._id || PAYLOAD.guestId;

    // Sum up all prices and store in variable
    let amount = 0;

    for(let i = 0; i < req.body.products.length; i++) {
        amount += req.body.products[i].line_total.raw;
    }
    
    amount = amount * 100;
    amount = Math.ceil(amount);
    console.log(amount)

    if(amount == 0) {
        return res.json({ message: "Warenkorb ist leer!" });
    }

    let completedOrder = new CompletedOrder({
        userId: USERID,
        details: req.body,
        completionDate: new Date(Date.now()),
        orderType: "Einkauf"
    });
    completedOrder.save();


    if(req.body.method === "stripe") {
        // STRIPE
        const paymentIntent = await stripe.paymentIntents.create({
            amount: amount,
            currency: 'eur'
        });

        return res.json({
            clientSecret: paymentIntent.client_secret
        });
    }
    
    if(req.body.method === "cash") {
        // log data about payment (amount, who, orderid) in case of trouble
        let productString = "";
        for(let i = 0; i < req.body.products.length; i++) {
            productString += ` ${req.body.products[i].quantity } ${req.body.products[i].product_name},`;
        }
        console.log(productString);
        
        informDriver(`Eine neue Bestellung! Fahren Sie zu Rewe und kaufen Sie folgende Lebensmittel: ${ productString }. Und bringen Sie sie zu: ${req.body.address}`);

        return res.json({ method: "cash", success: true });
    } 

    // Response
});

/* This Route is called by Frontend when Stripe completes purchase */
router.post('/completed', authenticate, (req, res) => {
    let productString = "";
    for(let i = 0; i < req.body.products.length; i++) {
        productString += ` ${req.body.products[i].quantity } ${req.body.products[i].product_name},`;
    }
    console.log(productString);

    informDriver(`Eine neue Bestellung! Fahren Sie zu Rewe und kaufen Sie folgende Lebensmittel: ${ productString }. Und bringen Sie sie zu: ${req.body.address}`);
    res.json({ success: true, method: "stripe" });
});

module.exports = router;