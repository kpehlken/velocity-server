require('dotenv').config();
const accountSid = process.env.TWILIO_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

function sendSms(bodyText, fromNumber, toNumber) {
    client.messages.create({
        body: bodyText,
        from: fromNumber,
        to: toNumber
    })
    .then(() => console.log("SMS send to Driver"))
    .catch(err => console.log(err));
}

module.exports = sendSms;