const jwt = require('jsonwebtoken');
const sendSms = require('../apis/twilio');
const Driver = require('../models/Driver');

function informDriver(body) {
    let driverName = "Fahrer";
    let driverPhone = "";

    // Look up available Driver
    // Set Driver Vars
    driverPhone = "+4915751913926";     // DEV

    // Inform / Send Sms
    sendSms(
        `Hallo, ${driverName}! ${body}`,
        "+19419607429",
        driverPhone.toString()
    );

    return driverName;
}

module.exports = informDriver;