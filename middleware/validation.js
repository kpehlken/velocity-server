const Joi = require('joi');

/* Auth Validation Functions */

function validateRegistrationRequest(data) {
    const schema = Joi.object({
        firstname: Joi.string().min(1).max(20).required(),
        lastname: Joi.string().min(1).max(40).required(),
        password: Joi.string().min(6).required(),
        email: Joi.string().min(6).email().required(),
        address: Joi.string().min(3).required()
        // CARD INFO? for Payment info on account page
    });
    return schema.validate(data);
}

function validateLoginRequest(data) {
    const schema = Joi.object({
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required()
    });
    return schema.validate(data);
}

function validateLogoutRequest(data) {
    const schema = Joi.object({
        refreshToken: Joi.string().required()
    });
    return schema.validate(data);
}

function validateNewTokenRequest(data) {
    const schema = Joi.object({
        refreshToken: Joi.string().required()
    });
    return schema.validate(data);
}

/* Product Validation Functions */

function validateProductSearchRequest(data) {
    const schema = Joi.object({
        term: Joi.string().required()
    });
    return schema.validate(data);
}

function validateProductCategoryRequest(data) {
    const schema = Joi.object({
        category: Joi.string().required()
    });
    return schema.validate(data);
}

function validatePurchaseRequest(data) {
    const schema = Joi.object({
        method: Joi.string().required(),
        products: Joi.array().required(),
        address: Joi.string().min(3).max(60).required()
    });
    return schema.validate(data); 
}

/* Delivery Validation Functions */ 

function validateRetourRequest(data) {
    const schema = Joi.object({
        weight: Joi.number().max(200).required(),
        address: Joi.string().min(3).max(60).required()
    });
    return schema.validate(data);
}

function validateForgottenRequest(data) {
    const schema = Joi.object({
        item: Joi.string().min(2).max(60),
        location: Joi.string().max(200).required(),
        address: Joi.string().min(3).max(100).required(),
    });
    return schema.validate(data);
}

/*
    Not Ready
*/
function validateReceiptRequest(data) {
    const schema = Joi.object({
        location: Joi.string().max(200).required()
    });
    return schema.validate(data);
}

function validateCustomRequest(data) {
    const schema = Joi.object({
        address: Joi.string().min(3).max(100).required(),
        description: Joi.string().min(10).max(500).required()
    });
    return schema.validate(data);
}

function validateCourierRequest(data) {
    const schema = Joi.object({
        userAddress: Joi.string().min(3).max(100).required(),
        destAddress: Joi.string().min(3).max(100).required(),
        description: Joi.string().max(200).required(),
        weight: Joi.number().required(),
        addressInfo: Joi.string().max(200).required(),
        instantDelivery: Joi.boolean()
    });
    return schema.validate(data);
}
/* Driver Validation Functions */

module.exports = {
    validateRegistrationRequest,
    validateLoginRequest,
    validateLogoutRequest,
    validateNewTokenRequest,
    validateProductSearchRequest,
    validateProductCategoryRequest,
    validateCourierRequest,
    validatePurchaseRequest,
    validateRetourRequest,
    validateForgottenRequest,
    validateReceiptRequest,
    validateCustomRequest
}