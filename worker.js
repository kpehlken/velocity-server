const mongoose = require('mongoose');
const schedule = require('node-schedule');
const io = require('socket.io');
const Order = require('./models/Order');

require('dotenv').config();

const firstDeliveyTime = "0 0 12 * * *";
const secondDeliveryTime = "0 0 18 * * *";

mongoose.connect(process.env.DB_URI, { useNewUrlParser: true, useFindAndModify: false }, () => {
    console.log("-.- Worker Connected To MongoDB");
});

async function getAllDeliveries() {
    let orders = await Order.find({});
    Order.deleteMany({}).then(() => {
        console.log("-.- Cleaned up Order Collection");
    });
    console.log(orders);
}

const firstJob = schedule.scheduleJob(firstDeliveyTime, () => {
    getAllDeliveries();
});

const secondJob = schedule.scheduleJob(secondDeliveryTime, () => {
    getAllDeliveries()
});

