const mongoose = require('mongoose');

const tokenSchema = mongoose.Schema({
    refreshToken: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model("Tokens", tokenSchema);