const mongoose = require('mongoose');

const DriverSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    availability: {
        type: Boolean,
        required: true
    },
    deliveriesDone: {
        type: Number,
        required: true
    },
    phoneNumber: {
        type: String,
        required: true
    }
});


module.exports = mongoose.model("Driver", DriverSchema);