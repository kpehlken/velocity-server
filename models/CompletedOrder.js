const mongoose = require('mongoose');

const completedOrderSchema = mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    details: {
        type: Object,
        required: true
    },
    completionDate: {
        type: Date,
        required: true
    },
    orderType: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model("CompletedOrder", completedOrderSchema);