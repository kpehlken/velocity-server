const mongoose = require('mongoose');

const DeliverySchema = mongoose.Schema({
    type: {
        type: String,
        required: true
    },
    orderAt: {
        type: Date,
        required: true
    },
    details: {
        type: Object,
        required: true
    }
});


module.exports = mongoose.model("Order", DeliverySchema);